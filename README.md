hyperres_gw_tools
==============================

Postprocessing tools to postprocess data from Jarno Verkaiks mega model

Manual installation
-------------------
1. Installation of all dependencies of the workflow is best done by
first installing 
[miniconda](https://docs.conda.io/en/latest/miniconda.html)
2. Install [git](https://git-scm.com/downloads) 
3. Optional:  [Imagemagick](https://imagemagick.org/script/download.php#windows) 
(For workflow graphs) Ensure during the installation that Imagemagick 
is added to the Windows PATH variable. 

Then in Anaconda Prompt (or other shell), type:

```console
conda activate base
conda install --channel=conda-forge mamba
```

This will install [mamba](https://github.com/mamba-org/mamba#readme), 
which is faster implementation of conda, in the base environment.

Cloning the repository
----------------------
We are cloning the repository from Gitlab with git as follows:

```console
git clone https://gitlab.com/JoerivanEngelen/hyperresolution-groundwater-tools.git
```

Installing dependencies
-----------------------

You can then install all necessary dependencies in a seperate conda 
environment as follows: 

```console
cd hyperresolution-groundwater-tools
mamba env create -f environment.yml
```

And then call to activate your environment:

```console
conda activate hyperres
```

We also need to do a development install of a specific imod-python branch required:

```console
cd ..
git clone https://gitlab.com/deltares/imod/imod-python.git
git checkout fix_allocation_budgets
cd imod-python
pip install -e .
```

Data
----
Copy the data from here, you need access to the Deltares network drive for this

```console
cd ../hyperresolution-groundwater-tools
cp -r n:\Deltabox\Postbox\Engelen,van Joeri\data .
```

Running the workflow
--------------------

To run the workflow, run:

```console
snakemake
```


Project Organization
--------------------

    .
    ├── AUTHORS.md
    ├── LICENSE
    ├── README.md
    ├── bin                 <- Your compiled model code can be stored here (not tracked by git)
    ├── config              <- Configuration files, e.g., for doxygen or for your model if needed
    ├── data                
    │   ├── 1-external      <- Data external to the project.
    │   ├── 2-interim       <- Intermediate data that has been altered.
    │   ├── 3-input         <- The processed data sets, ready for modeling.
    │   ├── 4-output        <- Data dump from the model.
    │   └── 5-visualization <- Post-processed data, ready for visualisation.
    ├── docs                <- Documentation, e.g., doxygen or scientific papers (not tracked by git)
    ├── notebooks           <- Jupyter notebooks
    ├── reports             <- For a manuscript source, e.g., LaTeX, Markdown, etc., or any project reports
    │   └── figures         <- Figures for the manuscript or reports
    └── src                 <- Source code for this project
        ├── 0-setup         <- Install necessary software, dependencies, pull other git projects, etc.
        ├── 1-prepare       <- Scripts and programs to process data, from 1-external to 2-interim.
        ├── 2-build         <- Scripts to create model specific inputm from 2-interim to 3-input. 
        ├── 3-model         <- Scripts to run model and convert or compress model results, from 3-input to 4-output.
        ├── 4-analyze       <- Scripts to post-process model results, from 4-output to 5-visualization.
        └── 5-visualize     <- Scripts for visualisation of your results, from 5-visualization to ./report/figures.