import geopandas as gpd
import pandas as pd
from pathlib import Path
from shapely.geometry import LineString

#%% Path management
#data_dir = Path(r"c:\src\hyperres_gw_tools") / "data"
#
#path_budget_shp = data_dir / "4-output"  / "budgets.shp"
#path_centroids  = data_dir / "4-output"  / "aqf_centroids.shp"
#
#path_budget_arrow = data_dir / "4-output"  / "budget_arrows.shp"

path_budget_shp = snakemake.input.path_budget_shp
path_centroids = snakemake.input.path_centroids
path_budget_arrow = snakemake.output.path_budget_arrow

#%%Settings

gcs = 100 * 8.333333e-3

#%% Read
df_budget = gpd.read_file(path_budget_shp)
centroids = gpd.read_file(path_centroids)

# %% Collect points for vector lines
centroids = centroids.set_index("index")
start_points = centroids.loc[df_budget["id_out"].values].geometry
end_points = centroids.loc[df_budget["id_in"].values].geometry

mid_points = df_budget.centroid

# %% Calculate vector lines
lines = [LineString([start_points.iloc[r], mid_points.iloc[r], end_points.iloc[r]]) for r in range(len(start_points))]

#Set geometry
df_budget.geometry = lines

# %% Clip off start points/end points with a buffer
blank_start = start_points.buffer(gcs).reset_index(drop=True)
blank_end = end_points.buffer(gcs).reset_index(drop=True)

centroid_to_centroid = df_budget.geometry

#Mask centroids
df_budget.geometry = df_budget.difference(blank_start).difference(blank_end)

#If empty, replace it with centroid to centroid line again
df_budget.geometry =  df_budget.geometry.where(~df_budget.geometry.is_empty, centroid_to_centroid)

#%% Last calculations
df_budget["tba_[km_y-1]"] = df_budget["tba_budget"] / 1e9 * 365


#%% Save to file
df_budget.to_file(path_budget_arrow)

