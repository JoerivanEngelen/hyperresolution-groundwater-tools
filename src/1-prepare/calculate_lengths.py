import numpy as np
import xarray as xr
from pathlib import Path
import rioxarray

#%%Calculate cell dimensions

def haversine(lat1, lat2, dlon, dlat):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    with haversine.
    """

    a = np.sin(dlat/2)**2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon/2)**2
    c = 2 * np.arcsin(np.sqrt(a)) 
    # Multiply with radius of earth in meters, split into seperate integers
    # To prevent numpy from broadcasting to np.float64
    return c * 6371 * 1000 

if __name__=="__main__":
    #%% Path management
    data_dir = Path(r"c:\src\hyperres_gw_tools") / "data"

#    latlon_path = data_dir / "2-interim" / "latlon.nc"
#    length_path = data_dir / "2-interim" / "cellsizes.nc"     

    path_latlon = snakemake.input.path_latlon
    path_length = snakemake.output.path_length

    #%% Process
    latlon = xr.open_dataset(path_latlon, chunks = "auto")
    
    lat = latlon["lat"]
    lon = latlon["lon"]

    lat2 = lat.roll({"latitude" : 1}, roll_coords = False)

    dlat = lat - lat2
    dlon = lon - lon.roll({"longitude" : 1}, roll_coords = False)

    length = xr.Dataset()

    length["lon"] = haversine(lat, lat, dlon, 0)
    length["lat"] = haversine(lat, lat2, 0, dlat)

    #Fix incorrect length on at the north pole
    length["lat"][0] = length["lat"][1]

    #%% Write
    length = length.rio.write_crs("epsg:4326")
    length = length.rio.write_transform()

    length.to_netcdf(path_length)

# %%
