import xarray as xr
import numpy as np
from pathlib import Path
import rioxarray
import os
import gc

#%%

def _merge_subdomains(pathlist):
    """
    Simplified from https://gitlab.com/deltares/imod/imod-python/-/blob/master/imod/idf.py
    """

    das = [xr.open_rasterio(path) for path in pathlist]

    x = np.unique(np.concatenate([da.x.values for da in das]))
    y = np.unique(np.concatenate([da.y.values for da in das]))

    nrow = y.size
    ncol = x.size

    out = np.full((nrow, ncol), np.nan, dtype=np.float32)

    for da in das:
        ix = np.searchsorted(x, da.x.values[0], side="left")
        iy = nrow - np.searchsorted(y, da.y.values[0], side="right")
        ysize, xsize = da.shape[-2:]
        out[iy : iy + ysize, ix : ix + xsize] = da.values

    coords = {"longitude" : x, "latitude" : y[::-1]}
    dims = ["latitude", "longitude"]

    # Garbage collect to prevent memory leak
    # https://github.com/pydata/xarray/issues/2186#issuecomment-1035611864
    for da in das:
        da.close()
    del das
    gc.collect()

    return xr.DataArray(out, coords=coords, dims=dims)

def create_merged_dataset(var, pathlist):
    ds = xr.Dataset()
    ds[var] = _merge_subdomains(pathlist)

    print(var)
    ds.rio.write_crs("epsg:4326", inplace=True)
    ds.rio.write_transform(inplace=True)

    return ds

#%% Path management
##Uncomment block for interactive runs
#data_dir = Path(r"c:\src\hyperres_gw_tools") / "data"
#input_folders = list((data_dir / "1-external" / "input_map").glob(r"tile_[0-9][0-9][0-9]-163\steady-state_only\maps"))
#
#vars = [
#    "horizontal_conductivity_lowermost_layer", 
#    "horizontal_conductivity_uppermost_layer",
#    "top_uppermost_layer",
#    "bottom_uppermost_layer",
#    "bottom_lowermost_layer",
#    ]
#
#ext = ".map"
#
#input_netcdf_folder = data_dir / "2-interim" / "input_map"
#
#paths = [[folder / (var + ext) for folder in input_folders] for var in vars ]

#Comment out this block for interactive runs
vars = snakemake.params.vars
paths = snakemake.input.paths

output_netcdfs = snakemake.output.output_netcdfs

paths = [paths[i::len(vars)] for i in range(len(vars))]

input_netcdf_folder = Path(output_netcdfs[0]) / ".."

#os.makedirs(input_netcdf_folder, exist_ok=True)
#os.makedirs(r"c:\src\hyperres_gw_tools\data\2-interim\input_map", exist_ok=True)

#%%
for var, pathlist, netcdf in zip(vars, paths, output_netcdfs):

    ds = create_merged_dataset(var, pathlist)

    ds.to_netcdf(netcdf)
    ds.close()

