import geopandas as gpd
import imod
import itertools
from pathlib import Path
import xarray as xr
import numpy as np
import pandas as pd
import scipy
from tqdm import tqdm

#TODO: Add to snakemake workflow
#TODO: Create map with flows
# https://plugins.qgis.org/plugins/FlowMapper/
# https://gogeomatics.ca/mapping-migration-flows-with-qgis-flowmapper-plugin/
# https://anitagraser.com/2016/12/30/new-style-flow-map-arrows/


def sel_from_gdf(da, gdf):
    """Select da based on extent in gdf"""
    xmin, ymin, xmax, ymax = gdf.geometry.total_bounds
    return da.sel(x=slice(xmin, xmax), y=slice(ymax, ymin))


#%% Path management
#data_dir = Path(r"c:\src\hyperres_gw_tools") / "data"

#path_tba_shp = data_dir / "1-external"  / "twap_aquiferdata_tbadelineation" / "twap_aquiferdata_tbadelineationPolygon.shp"
#path_fluxes = data_dir / "4-output"  / "fluxes.nc"
#rch_budget_nc = data_dir / "2-interim"  / "recharge.nc"
#
#path_budget_csv = data_dir / "4-output"  / "budgets.csv"
#path_budget_shp = data_dir / "4-output"  / "budgets.shp"
#path_centroids  = data_dir / "4-output"  / "aqf_centroids.shp"

path_tba_shp = snakemake.input.path_tba_shp
path_fluxes = snakemake.input.path_fluxes
rch_budget_nc = snakemake.input.rch_budget_nc

path_budget_csv = snakemake.output.path_budget_csv
path_budget_shp = snakemake.output.path_budget_shp
path_centroids = snakemake.output.path_centroids

#%% Read files
tbas_df = gpd.read_file(path_tba_shp)
fluxes = xr.open_dataset(path_fluxes, chunks="auto")
rch = xr.open_dataset(rch_budget_nc, chunks="auto")

# Convert original index to columnn number for rasterize
# and to do bookkeeping of zones
# We index 1-based, because currently
# imod.evaluate.budget.facebudget
# does not deal well with zone_nr = 0,
# UPDATE: Probably fixed in:
# https://gitlab.com/deltares/imod/imod-python/-/merge_requests/119
tbas_df = tbas_df.reset_index()
tbas_df["index"]

#%% Drop overall transboundary aquifer data
tbas_df = tbas_df.loc[tbas_df["country_co"] != "TBA"]

#%% Rename coordinates to "x" and "y"
fluxes = fluxes.rename(longitude = "x", latitude = "y")
rch = rch.rename(longitude = "x", latitude = "y")

#%%
df_budget = {
    "aq_code" : [], 
    "id_in" : [],
    "id_out" : [],
    "tba_budget" : [],
    "rch_source_zone" : [],
}

#%%Process
aq_codes = tbas_df["aq_code"].unique()


for aq_code in tqdm(aq_codes):
    tba_df = tbas_df.loc[tbas_df["aq_code"] == aq_code]

    fluxes_sel = sel_from_gdf(fluxes, tba_df)
    fluxes_sel = fluxes_sel.compute()

    rch_sel = sel_from_gdf(rch, tba_df)
    rch_sel.load()

    like = fluxes_sel["flux_x"].sel(layer=2).drop("layer")

    tba_raster = imod.prepare.spatial.rasterize(
        tba_df, like, column="index"
        )
    
    tba_raster = tba_raster.expand_dims(layer=[1,2])

    active = np.isfinite(tba_raster)
    fluxes_sel = fluxes_sel.where(active)
    rch_sel = rch_sel.where(active.sel(layer=2))

    zone_nrs = tba_df["index"]
    zone_combos = list(itertools.combinations(zone_nrs, 2))

    for combo in zone_combos:
        #Further select to reduce data to rasters per combo
        combo_df = tba_df.loc[tba_df["index"].isin(combo)]
        combo_raster = sel_from_gdf(tba_raster, combo_df)
        combo_zones = combo_raster.isin(combo)

        fluxes_combo = sel_from_gdf(fluxes_sel, combo_df).where(combo_zones)
        
        zone = combo_raster.where(combo_raster == combo[0])

        #Store cells of combo_zone without outer edge, to later remove the outer edge of the zone
        no_outer_edge = scipy.ndimage.morphology.binary_erosion(
            combo_zones.sel(layer=2) > 0
            )

        df_budget["aq_code"].append(aq_code)

        budget = imod.evaluate.budget.facebudget(
            zone, 
            front = fluxes_combo["flux_y"], 
            right = fluxes_combo["flux_x"]
            )
        
        budget = budget.where(no_outer_edge).sum()
        budget.load()

        if budget < 0:
            #Flow direction is reversed:
            # Recharge zone on the other side
            # ID's different direction
            rch_zone = combo_raster.where(combo_raster == combo[1])
            df_budget["id_in"].append(combo[1])
            df_budget["id_out"].append(combo[0])
            budget *= -1
        else:
            rch_zone = zone
            df_budget["id_in"].append(combo[0])
            df_budget["id_out"].append(combo[1])
        #Undo broadcasting
        rch_zone = rch_zone.sel(layer=2)

        rch_source = sel_from_gdf(rch_sel, combo_df).where(rch_zone)

        df_budget["tba_budget"].append(budget.values)
        df_budget["rch_source_zone"].append(rch_source["rch"].sum().values)

#%% Create DataFrame
df_budget = pd.DataFrame(df_budget)
#For interactive processing
df_budget_backup = df_budget.copy()

#%%
#Calculate fraction of net recharge water that flows across border
df_budget["tba_rch"] = df_budget["tba_budget"].abs()/df_budget["rch_source_zone"]

#Remove inactive cells
df_budget = df_budget.loc[df_budget["tba_rch"]>0]

#Calculate midpoints of polygon centroids
geo_in = gpd.GeoDataFrame(df_budget.join(tbas_df["geometry"], on = "id_in"))
geo_out = gpd.GeoDataFrame(df_budget.join(tbas_df["geometry"], on = "id_out"))

gcs = 5 * 8.333333e-3

midpoints = geo_in.buffer(gcs).intersection(geo_out.buffer(gcs))

df_budget = gpd.GeoDataFrame(df_budget.assign(geometry = midpoints))

#np.log10(df_budget["tba_rch"].astype(np.float64)).hist(bins=40)

#%% Store these for bookkeeping
df_budget = df_budget.reset_index(drop=True)

df_budget["region_in"] = tbas_df["region_cod"].loc[df_budget["id_in"]].reset_index(drop=True)
df_budget["region_out"] = tbas_df["region_cod"].loc[df_budget["id_out"]].reset_index(drop=True)

#%% Do not encode as object arrays, Fiona hates this
df_budget["tba_budget"] = df_budget["tba_budget"].astype(np.float64)
df_budget["rch_source_zone"] = df_budget["rch_source_zone"].astype(np.float64)
df_budget["tba_rch"] = df_budget["tba_rch"].astype(np.float64)
df_budget["aq_code"] = df_budget["aq_code"].astype(str)
df_budget["region_in"] = df_budget["region_in"].astype(str)
df_budget["region_out"] = df_budget["region_out"].astype(str)

#%% Calculate logarithm for plotting
df_budget["tba_budget_log"] = np.log10(df_budget["tba_budget"])

#%%Save
df_budget.to_csv(path_budget_csv)
df_budget.to_file(path_budget_shp)

#%% Save centroids
centroids = gpd.GeoDataFrame(geometry=tbas_df.geometry.centroid)
centroids = centroids.reset_index()
centroids.to_file(path_centroids)

# %%
