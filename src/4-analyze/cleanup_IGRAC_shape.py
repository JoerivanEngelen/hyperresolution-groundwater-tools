import geopandas as gpd
from pathlib import Path

#%%Path management
data_dir = Path(r"c:\src\hyperres_gw_tools") / "data"

# path_tba_shp = data_dir / "1-external"  / "twap_aquiferdata_tbadelineation" / "twap_aquiferdata_tbadelineationPolygon.shp"
# path_tba_shp_cleaned = data_dir / "4-output" / "tba_delineation.shp"

path_tba_shp = snakemake.input.path_tba_shp
path_tba_shp_cleaned = snakemake.input.path_tba_shp_cleaned

#%% Read data
tbas_df = gpd.read_file(path_tba_shp)

#%% Drop overall transboundary aquifer data
tbas_df = tbas_df.loc[tbas_df["country_co"] != "TBA"]

#%% Save
tbas_df.to_file(path_tba_shp_cleaned)