from pathlib import Path
import xarray as xr
import rioxarray
import numpy as np

#%% Path management
#data_dir = Path(r"c:\src\hyperres_gw_tools") / "data"

#lengths_nc = data_dir / "2-interim" / "cellsizes.nc"
#rch_inp_nc = data_dir / "2-interim" / "input_map" / "net_RCH.nc"
#
#rch_budget_nc = data_dir / "2-interim" / "recharge.nc"

lengths_nc = snakemake.input.lengths_nc
rch_inp_nc = snakemake.input.rch_inp_nc
rch_budget_nc = snakemake.output.rch_budget_nc

#%%Read
cellsizes = xr.open_dataset(lengths_nc, chunks="auto")
rch = xr.open_dataset(rch_inp_nc, chunks="auto")

#%% From Sutanudjaja 2011
# q_rch,inp = q_rch,act * (A_cell / A_MF)
# Where:
# q_rch,inp = recharge input in the model (m/d)
# q_rch,act = actual calculated recharge (m/d)
# A_cell = cell area (m^2)
# A_MF = Modflow cell area (m^2), equal to 30" X 30"
# So to we need:
# Q_rch,act = q_rch,inp * (A_MF/A_cell) * A_cell
# Q_rch,act = q_rch,inp * A_MF

d_lon = rch.longitude[1] - rch.longitude[0]
d_lat = rch.latitude[0] - rch.latitude[1]

A_MF = d_lon * d_lat

rch_budget = xr.Dataset()
rch_budget["rch"] = (rch["net_RCH"] * A_MF).astype(np.float32)

rch_budget.rio.write_crs("epsg:4326", inplace=True)
rch_budget.rio.write_transform(inplace=True)

rch_budget.to_netcdf(rch_budget_nc)

# %%
