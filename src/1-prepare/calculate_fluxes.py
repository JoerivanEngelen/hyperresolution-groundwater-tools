#%%
import xarray as xr
import numpy as np
import rioxarray
from pathlib import Path

import dask

#%%Set single-threaded
#dask.config.set(scheduler='single-threaded')

#%%Path management
#data_dir = Path(r"c:\src\hyperres_gw_tools") / "data"
#
#lengths_nc = data_dir / "2-interim" / "cellsizes.nc"
#thickness_nc = data_dir / "2-interim" /  "thickness.nc"
#gradient_nc = data_dir / "2-interim" / "gradient.nc"
#k_hor_nc = data_dir / "2-interim" / "k_hor.nc"
#
#fluxes_nc_y = data_dir / "4-output"  / "fluxes_y.nc"
#fluxes_nc_x = data_dir / "4-output"  / "fluxes_x.nc"
#
#fluxes_nc = data_dir / "4-output"  / "fluxes.nc"

lengths_nc = snakemake.input.lengths_nc
thickness_nc = snakemake.input.thickness_nc
gradient_nc = snakemake.input.gradient_nc
k_hor_nc = snakemake.input.k_hor_nc

fluxes_nc_y = snakemake.output.fluxes_y_nc
fluxes_nc_x = snakemake.output.fluxes_x_nc

fluxes_nc = snakemake.output.fluxes_nc

#%% Read
length = xr.open_dataset(lengths_nc, chunks="auto")
thickness = xr.open_dataset(thickness_nc, chunks="auto")
gradient = xr.open_dataset(gradient_nc, chunks="auto")
k_hor = xr.open_dataset(k_hor_nc, chunks="auto")


#%% Calculate and dump to disk

# Multiply with -1 beforehand as fluxes require a multiplication by -1
negative_transmissivity = k_hor["k_horizontal"] * thickness["thickness"] * -1

fluxes_y = gradient["dhead_dy"] * negative_transmissivity * length["lon"]
fluxes_y.to_netcdf(fluxes_nc_y)

fluxes_x = gradient["dhead_dx"] * negative_transmissivity * length["lat"]
fluxes_x.to_netcdf(fluxes_nc_x)

#%% Combine to one file
fluxes = xr.Dataset()

varname = "__xarray_dataarray_variable__"

fluxes["flux_x"] = xr.open_dataset(fluxes_nc_x, chunks="auto")[varname]
fluxes["flux_y"] = xr.open_dataset(fluxes_nc_y, chunks="auto")[varname]

fluxes = fluxes.rio.write_crs("epsg:4326")
fluxes = fluxes.rio.write_transform()

fluxes.to_netcdf(fluxes_nc)

# %%
