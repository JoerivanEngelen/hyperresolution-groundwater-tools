import geopandas as gpd
import imod
import itertools

path_tba_shp = r"c:\Users\engelen\projects_wdir\mf6global\plot_data\twap_aquiferdata_tbadelineation\twap_aquiferdata_tbadelineationPolygon.shp"

tba_df = gpd.read_file(path_tba_shp)

aq_codes = tba_df["aq_code"].unique()

for aq_code in aq_codes:
    tba_sel = tba_df.loc[tba_df["aq_code"] == aq_code]

    xmin, ymin, xmax, ymax = tba_sel.geometry.total_bounds

    fluxes_sel = fluxes.sel(longitude=slice(xmin, xmax), latitude=slice(ymax, ymin))
    fluxes_sel.compute()

    # Convert original index to columnn number for rasterize
    tba_sel = tba_sel.reset_index()

    tba_raster = imod.prepare.spatial.rasterize(tba_sel, fluxes_sel, columns="index")

    zone_nrs = tba_sel["index"]
    zone_combos = itertools.combinations(zone_nrs, 2)
