import xarray as xr
import numpy as np
import rioxarray
from pathlib import Path

#%% Path management
#data_dir = Path(r"c:\src\hyperres_gw_tools") / "data"
#
#input_netcdf_folder = data_dir / "2-interim" / "input_map"
#
#vars = [
#    "top_uppermost_layer",
#    "bottom_uppermost_layer",
#    "bottom_lowermost_layer",
#    ]
#
#ext = ".nc"
#
#paths = [input_netcdf_folder / (var + ext) for var in vars]
#
#thicknesses_netcdf = data_dir / "2-interim" / "thickness.nc"

paths = snakemake.input.paths
vars = snakemake.params.vars
thicknesses_netcdf = snakemake.output.thicknesses_netcdf


#%%Read
ds_ls = [xr.open_dataset(path, chunks="auto")[var] for var, path in zip(vars, paths)]

#%%Calculate
interfaces = xr.concat(ds_ls, dim="layer")
thickness = xr.Dataset()
thickness["thickness"] = interfaces.diff(dim="layer").assign_coords(layer=[1,2]) * -1

#%%Write
thickness = thickness.rio.write_crs("epsg:4326")
thickness = thickness.rio.write_transform()

thickness.to_netcdf(thicknesses_netcdf)
