"""
Write lat and lon to netcdf grids which can be later 
used to calculate cell dimensions using dask chunking.
"""

import xarray as xr
import numpy as np
from pathlib import Path
import rioxarray

#%% Path management
data_dir = Path(r"c:\src\hyperres_gw_tools") / "data"

path_head = snakemake.input.path_global_head
path_latlon = snakemake.output.path_latlon

#%%
global_head = xr.open_dataset(path_head, chunks = "auto")

lon = global_head["longitude"]
lat = global_head["latitude"]

lon = np.deg2rad(lon).astype(np.float32)
lat = np.deg2rad(lat).astype(np.float32)

latlon = xr.full_like(global_head.isel(layer=0), np.nan, dtype=np.float32)
latlon = latlon.drop_vars("head")

yv, xv = np.meshgrid(lat, lon, indexing="ij")

latlon["lon"] = (["latitude", "longitude"], xv)
latlon["lat"] = (["latitude", "longitude"], yv)

latlon = latlon.rio.write_crs("epsg:4326")
latlon = latlon.rio.write_transform()

latlon.to_netcdf(path_latlon)
