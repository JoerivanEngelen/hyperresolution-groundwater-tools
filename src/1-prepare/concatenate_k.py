import xarray as xr
import numpy as np
from pathlib import Path
import rioxarray

#%%Path management
#data_dir = Path(r"c:\src\hyperres_gw_tools") / "data"
#
#input_netcdf_folder = data_dir / "2-interim" / "input_map"
#
#vars = [
#    "horizontal_conductivity_uppermost_layer", 
#    "horizontal_conductivity_lowermost_layer",
#    ]
#
#ext = ".nc"
#
#paths = [input_netcdf_folder / (var + ext) for var in vars]
#
#k_horizontal_netcdf = data_dir / "2-interim" / "k_hor.nc"

paths = snakemake.input.paths
vars = snakemake.params.vars
k_horizontal_netcdf = snakemake.output.k_horizontal_netcdf

#%%
ds_ls = [xr.open_dataset(path, chunks="auto")[var] for var, path in zip(vars, paths)]

k_horizontal = xr.Dataset()
k_horizontal["k_horizontal"] = xr.concat(ds_ls, dim = "layer").assign_coords(layer=[1,2])

#%%Write
#k_horizontal = k_horizontal.rio.write_crs("epsg:4326")
#k_horizontal = k_horizontal.rio.write_transform()

k_horizontal.to_netcdf(k_horizontal_netcdf)
