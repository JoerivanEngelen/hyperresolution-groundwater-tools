"""
Calculate gradients
"""
#%%
import xarray as xr
import numpy as np
import rioxarray
from pathlib import Path

#%% Path management
#data_dir = Path(r"c:\src\hyperres_gw_tools") / "data"
#
#path_length = data_dir / "2-interim" / "cellsizes.nc"     
#path_global_head = data_dir / "4-output" / "head.nc"
#path_gradient = data_dir / "2-interim" / "gradient.nc"

path_length = snakemake.input.path_length
path_global_head = snakemake.input.path_global_head

path_gradient = snakemake.output.path_gradient

#%% Open
cellsizes = xr.open_dataset(path_length, chunks="auto")
global_head = xr.open_dataset(path_global_head, chunks="auto")

head = global_head["head"]

dhead_y = head.roll({"latitude" : 1}, roll_coords = False).fillna(0.) - head
dhead_x = head.roll({"longitude" : 1}, roll_coords = False).fillna(0.) - head

#Fix incorrect dhead on the north pole
dhead_y[0] = dhead_y[1] 

gradient = xr.Dataset()

gradient["dhead_dy"] = np.divide(dhead_y, cellsizes["latitude"], dtype=np.float32)
gradient["dhead_dx"] = np.divide(dhead_x, cellsizes["longitude"], dtype=np.float32)

#%% Write
gradient = gradient.rio.write_crs("epsg:4326")
gradient = gradient.rio.write_transform()

gradient.to_netcdf(path_gradient)