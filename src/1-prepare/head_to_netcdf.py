#%% import
import numpy as np
import struct
from pathlib import Path
import dask
import dask.array as da
import xarray as xr
import rioxarray

#%% open
def header_node_mapping(path):
    """read header of node mapping and read as dict"""
    attrs = {}

    with open(path, "rb") as f:
        attrs["gicol_min"] = struct.unpack("i", f.read(4))[0]
        attrs["gicol_max"] = struct.unpack("i", f.read(4))[0]
        attrs["girow_min"] = struct.unpack("i", f.read(4))[0]
        attrs["girow_max"] = struct.unpack("i", f.read(4))[0]
        attrs["nodes"] = struct.unpack("i", f.read(4))[0]
        attrs["headersize"] = f.tell()
    return attrs


def read_node_mapping(path):
    attrs = header_node_mapping(path)
    nodes = attrs.pop("nodes")
    headersize = attrs.pop("headersize")

    with open(path, "rb") as f:
        f.seek(headersize)
        # ilay, irow, icol
        # ilay < 0: It is a constant head cell
        a = np.reshape(np.fromfile(f, np.dtype("i4"), nodes * 3), (nodes, 3))

    return a


def read_hds_ss(path, nodes):
    """read head file for steady-state result (1 timestep)"""
    with open(path, "rb") as f:
        # Skip header:
        # Record 1: KSTP,KPER,PERTIM,TOTIM,TEXT,NODES,1,1
        # With TEXT = "HEAD"
        # should be 52 bits
        f.seek(52, 1)
        a = np.fromfile(f, np.float64, nodes)
    return a


def map_subdomain_to_global_map(global_head, path_mapping, path_head):
    mapping = read_node_mapping(path_mapping)
    nodes = mapping.shape[0]

    head1d = read_hds_ss(path_head, nodes)

    # Filter out constant head cells (at boundaries)
    head1d = head1d[mapping[:, 0] > 0]
    mapping = mapping[mapping[:, 0] > 0]

    # Fortran 1-based indexing to Python's 0-based indexing
    mapping -= 1

    # For some reason fancy indexing with global_head[mapping]
    # results in wrong broadcasting into huge arrays (PiB)
    global_head[mapping[:, 0], mapping[:, 1], mapping[:, 2]] = head1d

    return global_head


def create_global_head_map(global_shape, paths_mapping, paths_head):
    # Allocate global array
    global_head = np.empty(global_shape, dtype=np.float32)
    global_head[:] = np.nan

    # Map subdomains to global map
    for path_mapping, path_head in zip(paths_mapping, paths_head):
        global_head = map_subdomain_to_global_map(global_head, path_mapping, path_head)

    return global_head


if __name__ == "__main__":
#    #%% Path management
#    data_dir = Path(r"c:\src\hyperres_gw_tools") / "data"
#
#    #TODO: Check if paths exist
#    paths_mapping = (data_dir / "1-external" / "models" / "post_mappings").glob("m*.nodmap.bin")
#    paths_head = (data_dir / "1-external" / "models" / "run_output_bin").glob("m*.ss.hds")
#
#    path_dummy = r"c:\src\hyperres_gw_tools\data\2-interim\input_map\horizontal_conductivity_uppermost_layer.nc"
#
#    path_netcdf = (data_dir / "4-output" / "head.nc")

    paths_mapping = snakemake.input.paths_mappings
    paths_head = snakemake.input.paths_bin
    path_dummy = snakemake.input.path_dummy

    path_netcdf = snakemake.output.path_global_head

    #%% Global extent
    ncol = 43200
    nrow = 21600
    nlay = 2
    xmin = -180
    ymin = -90
    gcs = 8.333333333333e-003

    global_shape = (nlay, nrow, ncol)

    xmax = xmin * -1
    ymax = ymin * -1

    x = np.linspace(xmin + gcs/2, xmax - gcs/2 , num=ncol)
    y = np.linspace(ymin + gcs/2, ymax - gcs/2, num=nrow)[::-1]

    coords = {"longitude": x, "latitude": y, "layer": [1, 2]}
    dims = ("layer", "latitude", "longitude")

    # TODO: For transient results, create global_head maps in loop
    # and dask stack them; in pseudo-code:
    # da_arrays = []
    # for paths_mapping, paths_head in zip(paths_mapping_grouped, paths_heads_grouped):
    #    dd = dask.delayed(create_global_head_map)(global_shape, paths_mapping, paths_head)
    #    global_head = da.from_delayed(dd, global_shape, dtype=np.float16)
    #    da_arrays.append(global_head)
    # full_da = dask.stack(da_arrays)
    # xr.DataArray(fullda, coords, dims)

    dd = dask.delayed(create_global_head_map)(global_shape, paths_mapping, paths_head)
    global_head = da.from_delayed(dd, global_shape, dtype=np.float32)

    global_head = xr.Dataset(data_vars = {"head" : (dims, global_head)}, coords=coords)

    dummy = xr.open_dataset(path_dummy, chunks="auto")

    #%% Clip off sounth pole, which is not in input data
    global_head = global_head .sel(latitude=slice(None, -60.))

    #%% Reindex to correct for small differences (<2e-14) in lot and lat coordinates
    global_head = global_head.assign_coords(latitude = dummy["latitude"], longitude = dummy["longitude"])

    global_head = global_head.rio.write_crs("epsg:4326")
    global_head = global_head.rio.write_transform()

    #%% Write
    global_head.to_netcdf(path_netcdf)


# %%
