from pathlib import Path
import numpy as np

to_merge_vars = [
    "horizontal_conductivity_lowermost_layer", 
    "horizontal_conductivity_uppermost_layer",
    "top_uppermost_layer",
    "bottom_uppermost_layer",
    "bottom_lowermost_layer",
    "net_RCH",
    "abstraction_uppermost_layer",
    "abstraction_lowermost_layer",
    ]

tiles = ["{:03d}".format(i) for i in range(1, 164)]

interface_vars = [    
    "top_uppermost_layer",
    "bottom_uppermost_layer",
    "bottom_lowermost_layer",
    ]

k_vars = [
    "horizontal_conductivity_uppermost_layer", 
    "horizontal_conductivity_lowermost_layer",
    ]

model_numbers = ["{:05d}".format(i) for i in range(1, 514)]

snakedir = workflow.basedir

###############################RULES##########################

rule all:
    input:
        "data/4-output/tba_delineation.shp",
        "data/4-output/budget_arrows.shp"

rule unzip_input_map:
    input:
        "data/1-external/input_map/ss_030321.zip"
    output:
        expand("data/1-external/input_map/tile_{tiles}-163/steady-state_only/maps/{vars}.map", tiles=tiles, vars=to_merge_vars)
    shell:
        "7z x {input} -o{snakedir}/data/1-external/input_map -r"

rule unzip_heads:
    input:
        "data/1-external/models/run_output_bin/ss.hds.zip"
    output:
        expand("data/1-external/models/run_output_bin/m{modnr}.ss.hds", modnr = model_numbers)
    shell:
        "7z x {input} -o{snakedir}/data/1-external/models/run_output_bin -r"

rule unzip_head_mappings:
    input:
        "data/1-external/models/post_mappings/mod_post_map.zip"
    output:
        expand("data/1-external/models/post_mappings/m{modnr}.nodmap.bin", modnr = model_numbers)
    shell:
        "7z x {input} -o{snakedir}/data/1-external/models/post_mappings -r"

rule draw_flux_lines:
    input:
        path_budget_shp = "data/4-output/budgets.shp",
        path_centroids = "data/4-output/aqf_centroids.shp"
    output:
        path_budget_arrow = "data/4-output/budget_arrows.shp"
    script:
        "src/4-analyze/draw_flux_lines.py"


rule cleanup_IGRAC_shapes:
    input: 
        path_tba_shp = "data/1-external/twap_aquiferdata_tbadelineation/twap_aquiferdata_tbadelineationPolygon.shp",
    output:
        path_tba_shp_cleaned = "data/4-output/tba_delineation.shp"
    script:
        "src/4-analyze/cleanup_IGRAC_shape.py"

rule transboundary_fluxes:
    input:
        path_tba_shp = "data/1-external/twap_aquiferdata_tbadelineation/twap_aquiferdata_tbadelineationPolygon.shp",
        path_fluxes = "data/4-output/fluxes.nc",
        rch_budget_nc= "data/2-interim/recharge.nc",
    output:
        path_budget_csv = "data/4-output/budgets.csv",
        path_budget_shp = "data/4-output/budgets.shp",
        path_centroids = "data/4-output/aqf_centroids.shp",
    script:
        "src/4-analyze/calculate_transboundary_fluxes.py"


rule calculate_fluxes:
    input:
        lengths_nc = "data/2-interim/cellsizes.nc",
        gradient_nc = "data/2-interim/gradient.nc",
        thickness_nc = "data/2-interim/thickness.nc",
        k_hor_nc = "data/2-interim/k_hor.nc",
        script = "src/1-prepare/calculate_fluxes.py"
    output:
        fluxes_y_nc = temp("data/4-output/fluxes_y.nc"),
        fluxes_x_nc = temp("data/4-output/fluxes_x.nc"),
        fluxes_nc = "data/4-output/fluxes.nc"
    script:
        "src/1-prepare/calculate_fluxes.py"

rule calculate_gradients:
    input:
        path_length = "data/2-interim/cellsizes.nc",
        path_global_head = "data/4-output/head.nc",
        script = "src/1-prepare/calculate_gradients.py"
    output:
        path_gradient = "data/2-interim/gradient.nc",
    script:
        "src/1-prepare/calculate_gradients.py"

rule calculate_lengths:
    input:
        path_latlon = "data/2-interim/latlon.nc",
        script = "src/1-prepare/calculate_lengths.py"
    output:
        path_length = "data/2-interim/cellsizes.nc",
    script:
        "src/1-prepare/calculate_lengths.py"

rule write_latlon_grids:
    input:
        path_global_head = "data/4-output/head.nc",
        script = "src/1-prepare/write_latlon_grids.py"
    output:
        path_latlon = "data/2-interim/latlon.nc"
    script:
        "src/1-prepare/write_latlon_grids.py"

rule head_to_netcdf:
    input:
        path_dummy = "data/2-interim/input_map/{vars}.nc".format(vars=k_vars[0]),
        paths_mappings = expand("data/1-external/models/post_mappings/m{modnr}.nodmap.bin", modnr = model_numbers),
        paths_bin = expand("data/1-external/models/run_output_bin/m{modnr}.ss.hds", modnr = model_numbers),
        script = "src/1-prepare/head_to_netcdf.py"
    output:
        path_global_head = "data/4-output/head.nc",
    script:
        "src/1-prepare/head_to_netcdf.py"

rule merge_model_input:
    input:
        paths = expand("data/1-external/input_map/tile_{tiles}-163/steady-state_only/maps/{vars}.map", tiles=tiles, vars=to_merge_vars),
        script = "src/1-prepare/merge_model_input.py",
    params:
        vars = to_merge_vars,
    output:
        output_netcdfs = expand("data/2-interim/input_map/{vars}.nc", vars=to_merge_vars),
    script:
        "src/1-prepare/merge_model_input.py"

rule calculate_thicknesses:
    input:
        paths = expand("data/2-interim/input_map/{vars}.nc", vars=interface_vars),
        script = "src/1-prepare/calculate_thicknesses.py"
    params:
        vars = interface_vars,
    output:
        thicknesses_netcdf = "data/2-interim/thickness.nc",
    script:
        "src/1-prepare/calculate_thicknesses.py"

rule concatenate_k:
    input:
        paths = expand("data/2-interim/input_map/{vars}.nc", vars=k_vars)
    params:
        vars = k_vars
    output:
        k_horizontal_netcdf = "data/2-interim/k_hor.nc"
    script:
        "src/1-prepare/concatenate_k.py"

rule calculate_recharge_budget:
    input:
        lengths_nc = "data/2-interim/cellsizes.nc",
        rch_inp_nc = "data/2-interim/input_map/net_RCH.nc",
    output:
        rch_budget_nc = "data/2-interim/recharge.nc",
    script:
        "src/1-prepare/calculate_recharge_budget.py"